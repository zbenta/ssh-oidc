# ssh-oidc
Docker compose  files and entrypoint scripts to enable access to INCD's HPC cluster using SSH-OIDC

There are 2 environment variables you must alter in the `.env` file replace them with the token you wish to use and the host you wish to authenticate into:

```
TOKEN=egi
THEHOST=mysuperhost.com
```

You can use this solution as follows:

```
docker-compose run oidc

It will display the following menu:

Choose one option:
1 - Register;
2 - Connect;
3 - SCP;
0 - Exit;
```

The first thing to do is to register, we have defined egi as our default authentication method, feel free to use another one as per the [documentation](https://github.com/EOSC-synergy/ssh-oidc).

To register do the steps bellow:

![1](images/1.png)  



Use the provided link and insert the code that was given click on submit

![1](images/2.png) 

Authorise

![1](images/3.png) 
![1](images/4.png) 

Enter the desired password and you done

![1](images/5.png) 

Now you can choose to connet to our machine using option 2

![1](images/6.png) 

We have also added the option for uploading files throuhg scp, for that purpose please use option 3. We have created the folder THEDATA that maps to the internal /MYDATA folder on the container, so you can use these folder for this purpose.

![1](images/7.png) 

When you wish to exit the container, please choose option 0

![1](images/8.png)
