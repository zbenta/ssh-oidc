#!/bin/bash

########################################
#  LIP/INCD 2022                       #
#                                      #
#  AUTHORS:                            #
#  Cesar Ferreira - cesar@lip.pt       #
#  Zacarias Benta - zacarias@lip.pt    #
# ######################################

OPTION=99

while [ $OPTION -ne 0 ]
do       	

	echo "Choose one option:"
	echo "1 - Register;"
	echo "2 - Connect;"
	echo "3 - SCP;"
	echo "0 - Exit;"

	read OPTION

	case $OPTION in
	
		1)
		#start service
		eval `oidc-agent-service start`
		#register and authenticate
		oidc-gen --pub --issuer https://aai.egi.eu/auth/realms/egi  --scope "openid profile email offline_access eduperson_entitlement eduperson_scoped_affiliation eduperson_unique_id" $TOKEN
		;;

		2)
		#start service
		eval `oidc-agent-service start`
		#load profile
		oidc-add $TOKEN
		#connect
 		mccli ssh $THEHOST --oidc $TOKEN
		;;
		
		3)
		#start service
                eval `oidc-agent-service start`
                #load profile
                oidc-add $TOKEN
		OPTIONI=" "
		while [ "$OPTIONI" != "n"  ]
		do	
			echo "Insert path to file:"
			read PATHTOFILE
			mccli scp  $PATHTOFILE $THEHOST:~ --oidc $TOKEN
		echo " do you wish to uplçoad a new file? y/n"
		read OPTIONI
		done
		;;
	esac

done

